using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static LevelManagement;
using TMPro;
using System.Globalization;
using System;
using System.IO;

public class Buttons : MonoBehaviour
{
    private GameObject gameObjects;
    private GameObject[] buttons;
    private GameObject[] loadOrSave;
    private GameObject[] setParameters;
    private GameObject[] setParametersB;
    private GameObject[] laneButtons;
    private GameObject[] gameModeButtons;

    public void Sup_pro()
    {
        LevelManagement.gameMode = 1;
        gameModeString = "sup/pro";
        DestroyButtons();
    }

    public void Ulnar_radial()
    {
        LevelManagement.gameMode = 2;
        gameModeString = "ulnar/rad";
        DestroyButtons();
    }

    public void Flex_ex()
    {
        LevelManagement.gameMode = 3;
        gameModeString = "flex/ex";
        DestroyButtons();
    }

    public void LoadData()
    {
        SetArrayPath();
        LevelManagement.SetArray();

        loadOrSave = GameObject.FindGameObjectsWithTag("LoadOrSave");

        foreach (GameObject button in loadOrSave)
        {
            Destroy(button);
        }

        GoToNeutralPosition();
    }

    public void SaveData()
    {
        loadOrSave = GameObject.FindGameObjectsWithTag("LoadOrSave");

        foreach (GameObject button in loadOrSave)
        {
            Destroy(button);
        }

        setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

        foreach (GameObject button in setParameters)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the number of levels (1-50)";

        UpdateCountA();

    }

    #region +/- buttons
    public void PlusOneA()
    {
        startCounter += 1;

        UpdateCountA();
    }

    public void PlusFiveA()
    {
        startCounter += 5;

        UpdateCountA();
    }

    public void PlusTwentyA()
    {
        startCounter += 20;

        UpdateCountA();
    }

    public void MinusOneA()
    {
        startCounter -= 1;

        UpdateCountA();
    }

    public void MinusFiveA()
    {
        startCounter -= 5;

        UpdateCountA();
    }

    public void MinusTwentyA()
    {
        startCounter -= 20;

        UpdateCountA();
    }

    public void PlusOneB()
    {
        increment += 1;

        UpdateCountB();
    }

    public void PlusFiveB()
    {
        increment += 5;

        UpdateCountB();
    }

    public void PlusTwentyB()
    {
        increment += 20;

        UpdateCountB();
    }

    public void MinusOneB()
    {
        increment -= 1;

        UpdateCountB();
    }

    public void MinusFiveB()
    {
        increment -= 5;

        UpdateCountB();
    }

    public void MinusTwentyB()
    {
        increment -= 20;

        UpdateCountB();
    }
    #endregion

    public void UpdateCountA()
    {
        if (nextCounter == 10 || nextCounter == 4)
        {
            float f = (float)Mathf.Round((float)startCounter * 100f) / 1000f;
            GameObject.Find("text_start_Counter").GetComponent<TextMeshProUGUI>().text = "" + f;
        }
        else
        {
            GameObject.Find("text_start_Counter").GetComponent<TextMeshProUGUI>().text = "" + startCounter;
        }
    }

    public void UpdateCountB()
    {
        if (nextCounter == 10 || nextCounter == 4)
        {
            float f = (float)Mathf.Round((float)increment * 100f) / 1000f;
            GameObject.Find("text_Increment").GetComponent<TextMeshProUGUI>().text = "" + f;
        }
        else
        {
            GameObject.Find("text_Increment").GetComponent<TextMeshProUGUI>().text = "" + increment;
        }
    }

    public void Button2Lanes()
    {
        for (int i = 0; i < rows; i++)
            {
               levelParameters[i, 1] = 2;
            }

        nextCounter++;

        startCounter = 15;
        increment = 5;

        UpdateCountA();
        UpdateCountB();

        GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the movement angle for the left/downward direction (min 3)";

        laneButtons = GameObject.FindGameObjectsWithTag("button_lanes");

        foreach (GameObject button in laneButtons)
        {
            button.transform.localScale = new Vector3(0, 0, 0);
        }

        setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

        foreach (GameObject button in setParameters)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

        foreach (GameObject button in setParametersB)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void Button3Lanes()
    {
        for (int i = 0; i < rows; i++)
        {
            levelParameters[i, 1] = 3;
        }

        nextCounter++;

        startCounter = 15;
        increment = 5;

        UpdateCountA();
        UpdateCountB();

        GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the movement angle for the left/downward direction (min 3)";

        laneButtons = GameObject.FindGameObjectsWithTag("button_lanes");

        foreach (GameObject button in laneButtons)
        {
            button.transform.localScale = new Vector3(0, 0, 0);
        }

        setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

        foreach (GameObject button in setParameters)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

        foreach (GameObject button in setParametersB)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void Button4Lanes()
    {
        for (int i = 0; i < rows; i++)
        {
            levelParameters[i, 1] = 4;
        }

        nextCounter++;

        startCounter = 15;
        increment = 5;

        UpdateCountA();
        UpdateCountB();

        GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the movement angle for the left/downward direction (min 3)";

        laneButtons = GameObject.FindGameObjectsWithTag("button_lanes");

        foreach (GameObject button in laneButtons)
        {
            button.transform.localScale = new Vector3(0, 0, 0);
        }

        setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

        foreach (GameObject button in setParameters)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

        foreach (GameObject button in setParametersB)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    #region ButtonGamemodes
    public void Button1Gamemode()
    {
        for (int i = 0; i < rows; i++)
        {
            levelParameters[i, 8] = 1;
        }

        nextCounter++;

        GameObject.Find("text_feedback_mode").transform.localScale = new Vector3(0, 0, 0);

        GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the feedback force in %";

        setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

        setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

        foreach (GameObject button in setParameters)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        foreach (GameObject button in setParametersB)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        gameModeButtons = GameObject.FindGameObjectsWithTag("button_gamemodes");

        foreach (GameObject button in gameModeButtons)
        {
            button.transform.localScale = new Vector3(0, 0, 0);
        }

        startCounter = 80;
        increment = -5;

        UpdateCountA();
        UpdateCountB();
    }

    public void Button2Gamemode()
    {
        for (int i = 0; i < rows; i++)
        {
            levelParameters[i, 8] = 2;
        }

        nextCounter++;

        GameObject.Find("text_feedback_mode").transform.localScale = new Vector3(0, 0, 0);

        GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the feedback force in %";

        setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

        setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

        foreach (GameObject button in setParameters)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        foreach (GameObject button in setParametersB)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        gameModeButtons = GameObject.FindGameObjectsWithTag("button_gamemodes");

        foreach (GameObject button in gameModeButtons)
        {
            button.transform.localScale = new Vector3(0, 0, 0);
        }

        startCounter = 80;
        increment = -5;

        UpdateCountA();
        UpdateCountB();
    }

    public void Button3Gamemode()
    {
        for (int i = 0; i < rows; i++)
        {
            levelParameters[i, 8] = 3;
        }

        nextCounter++;

        GameObject.Find("text_feedback_mode").transform.localScale = new Vector3(0, 0, 0);

        GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the feedback force in %";

        setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

        setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

        foreach (GameObject button in setParameters)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        foreach (GameObject button in setParametersB)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        gameModeButtons = GameObject.FindGameObjectsWithTag("button_gamemodes");

        foreach (GameObject button in gameModeButtons)
        {
            button.transform.localScale = new Vector3(0, 0, 0);
        }

        startCounter = 80;
        increment = -5;

        UpdateCountA();
        UpdateCountB();
    }

    public void Button4Gamemode()
    {
        for (int i = 0; i < rows; i++)
        {
            levelParameters[i, 8] = 4;
        }

        nextCounter++;

        GameObject.Find("text_feedback_mode").transform.localScale = new Vector3(0, 0, 0);

        GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the feedback force in %";

        setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

        setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

        foreach (GameObject button in setParameters)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        foreach (GameObject button in setParametersB)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        gameModeButtons = GameObject.FindGameObjectsWithTag("button_gamemodes");

        foreach (GameObject button in gameModeButtons)
        {
            button.transform.localScale = new Vector3(0, 0, 0);
        }

        startCounter = 80;
        increment = -5;

        UpdateCountA();
        UpdateCountB();
    }

    public void Button5Gamemode()
    {
        for (int i = 0; i < rows; i++)
        {
            levelParameters[i, 8] = 5;
        }

        nextCounter++;

        GameObject.Find("text_feedback_mode").transform.localScale = new Vector3(0, 0, 0);

        GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the feedback force in %";

        setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

        setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

        foreach (GameObject button in setParameters)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        foreach (GameObject button in setParametersB)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
        }

        gameModeButtons = GameObject.FindGameObjectsWithTag("button_gamemodes");

        foreach (GameObject button in gameModeButtons)
        {
            button.transform.localScale = new Vector3(0, 0, 0);
        }

        startCounter = 80;
        increment = -5;

        UpdateCountA();
        UpdateCountB();
    }
    #endregion

    public void Next()
    {
        if (nextCounter == 0)
        {
            if (startCounter > 0 && startCounter <= 50)
            {
                rows = startCounter;

                GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the number of lanes ";

                startCounter = 3;

                setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

                foreach (GameObject button in setParameters)
                {
                    button.transform.localScale = new Vector3(0, 0, 0);
                }

                laneButtons = GameObject.FindGameObjectsWithTag("button_lanes");

                foreach (GameObject button in laneButtons)
                {
                    button.transform.localScale = new Vector3(1.10125f, 1.10125f, 1.10125f);
                }

                GameObject.Find("back").transform.localScale = new Vector3(1, 1, 1);

                UpdateCountA();
                UpdateCountB();

                nextCounter++;
            }
        }
        else if (nextCounter == 2)
        {
            if (startCounter > 2)
            {
                for (int i = 0; i < rows; i++)
                {
                    if (startCounter + (i * increment) < 3)
                    {
                        levelParameters[i, 2] = 3;
                    }
                    else if (startCounter + (i * increment) > 90 && (gameMode == 1 || gameMode == 3)) 
                    {
                        levelParameters[i, 2] = 90;
                    }
                    else if(startCounter + (i * increment) > 30 & gameMode == 2)
                    {
                        levelParameters[i, 2] = 30;
                    }
                    else
                    {
                        levelParameters[i, 2] = startCounter + (i * increment);
                    }
                }

                nextCounter++;

                GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the movement angle for the right/upward direction (min 3)";

            }
        }
        else if (nextCounter == 3)
        {
            if (startCounter > 2)
            {
                for (int i = 0; i < rows; i++)
                {
                    if (startCounter + (i * increment) < 3)
                    {
                        levelParameters[i, 3] = 3;
                    }
                    else if (startCounter + (i * increment) > 90 && (gameMode == 1 || gameMode == 3))
                    {
                        levelParameters[i, 3] = 90;
                    }
                    else if (startCounter + (i * increment) > 30 & gameMode == 2)
                    {
                        levelParameters[i, 3] = 30;
                    }
                    else
                    {
                        levelParameters[i, 3] = startCounter + (i * increment);
                    }
                }

                nextCounter++;

                GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the movement angle for the neutral position";

                GameObject.Find("+1A").GetComponentInChildren<Text>().text = "+0,1";
                GameObject.Find("+5A").GetComponentInChildren<Text>().text = "+0,5";
                GameObject.Find("+20A").GetComponentInChildren<Text>().text = "+2";
                GameObject.Find("-1A").GetComponentInChildren<Text>().text = "-0,1";
                GameObject.Find("-5A").GetComponentInChildren<Text>().text = "-0,5";
                GameObject.Find("-20A").GetComponentInChildren<Text>().text = "-2";

                GameObject.Find("+1B").GetComponentInChildren<Text>().text = "+0,1";
                GameObject.Find("+5B").GetComponentInChildren<Text>().text = "+0,5";
                GameObject.Find("+20B").GetComponentInChildren<Text>().text = "+2";
                GameObject.Find("-1B").GetComponentInChildren<Text>().text = "-0,1";
                GameObject.Find("-5B").GetComponentInChildren<Text>().text = "-0,5";
                GameObject.Find("-20B").GetComponentInChildren<Text>().text = "-2";

                startCounter = 30;
                increment = 0;

                UpdateCountA();
                UpdateCountB();
            }
        }
        else if (nextCounter == 4)
        {
            if (startCounter > 0)
            {
                for (int i = 0; i < rows; i++)
                {
                    if((float)startCounter / 10f + ((float)i * (float)increment / 10f) < 0.1f)
                    {
                        levelParameters[i, 4] = 0.1f;
                    }
                    else if((float)startCounter / 10f + ((float)i * (float)increment / 10f) > levelParameters[i,2] || (float)startCounter / 10f + ((float)i * (float)increment / 10f) > levelParameters[i, 3])
                    {
                        levelParameters[i, 4] = Math.Min(levelParameters[i, 2], levelParameters[i, 3]);
                    }
                    else
                    {
                        levelParameters[i, 4] = (float)startCounter / 10f + ((float)i * (float)increment / 10f);
                    }
                }

                nextCounter++;

                GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the speed of the car (min 1)";

                GameObject.Find("+1A").GetComponentInChildren<Text>().text = "+1";
                GameObject.Find("+5A").GetComponentInChildren<Text>().text = "+5";
                GameObject.Find("+20A").GetComponentInChildren<Text>().text = "+20";
                GameObject.Find("-1A").GetComponentInChildren<Text>().text = "-1";
                GameObject.Find("-5A").GetComponentInChildren<Text>().text = "-5";
                GameObject.Find("-20A").GetComponentInChildren<Text>().text = "-20";

                GameObject.Find("+1B").GetComponentInChildren<Text>().text = "+1";
                GameObject.Find("+5B").GetComponentInChildren<Text>().text = "+5";
                GameObject.Find("+20B").GetComponentInChildren<Text>().text = "+20";
                GameObject.Find("-1B").GetComponentInChildren<Text>().text = "-1";
                GameObject.Find("-5B").GetComponentInChildren<Text>().text = "-5";
                GameObject.Find("-20B").GetComponentInChildren<Text>().text = "-20";

                startCounter = 10;
                increment = 0;

                UpdateCountA();
                UpdateCountB();

            }
        }
        else if (nextCounter == 5)
        {
            if (startCounter > 0)
            {
                for (int i = 0; i < rows; i++)
                {
                    if(startCounter + (i * increment) < 1)
                    {
                        levelParameters[i, 5] = 1;
                    }
                    else
                    {
                        levelParameters[i, 5] = startCounter + (i * increment);
                    }
                }

                nextCounter++;

                GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the distance between obstacles (min 5)";

                startCounter = 60;
                increment = -1;

                UpdateCountA();
                UpdateCountB();

            }
        }
        else if (nextCounter == 6)
        {
            if (startCounter > 4)
            {
                for (int i = 0; i < rows; i++)
                {
                    if (startCounter + (i * increment) < 5)
                    {
                        levelParameters[i, 6] = 5;
                    }
                    else
                    {
                        levelParameters[i, 6] = startCounter + (i * increment);
                    }
                }

                nextCounter++;

                GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the time for the level in s (min 1)";

                startCounter = 100;
                increment = 0;

                UpdateCountA();
                UpdateCountB();

            }
        }
        else if (nextCounter == 7)
        {
            if (startCounter > 0)
            {
                for (int i = 0; i < rows; i++)
                {
                    if (startCounter + (i * increment) < 1)
                    {
                        levelParameters[i, 7] = 1;
                    }
                    else
                    {
                        levelParameters[i, 7] = startCounter + (i * increment);
                    }
                }

                nextCounter++;

                setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

                foreach (GameObject button in setParameters)
                {
                    button.transform.localScale = new Vector3(0,0,0);
                }

                setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

                foreach (GameObject button in setParametersB)
                {
                    button.transform.localScale = new Vector3(0, 0, 0);
                }

                gameModeButtons = GameObject.FindGameObjectsWithTag("button_gamemodes");

                foreach (GameObject button in gameModeButtons)
                {
                    button.transform.localScale = new Vector3(1, 1, 1);
                }

                GameObject.Find("back").transform.localScale = new Vector3(1, 1, 1);
                //GameObject.Find("text_feedback_mode").transform.localScale = new Vector3(1, 1, 1);
                
                GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the feedback mode";

                startCounter = 1;
                increment = 0;

                UpdateCountA();
                UpdateCountB();

            }
        }
        else if (nextCounter == 9)
        {
            if (startCounter >= 0 && startCounter <= 100)
            {
                for (int i = 0; i < rows; i++)
                {
                    if (startCounter + (i * increment) <= 0)
                    {
                        levelParameters[i, 9] = 0;
                    }
                    else if (startCounter + (i * increment) >= 100)
                    {
                        levelParameters[i, 9] = 100;
                    }
                    else
                    {
                        levelParameters[i, 9] = startCounter + (i * increment);
                    }
                }

                GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the time delay between force feedback in s";

                GameObject.Find("+1A").GetComponentInChildren<Text>().text = "+0,1";
                GameObject.Find("+5A").GetComponentInChildren<Text>().text = "+0,5";
                GameObject.Find("+20A").GetComponentInChildren<Text>().text = "+2";
                GameObject.Find("-1A").GetComponentInChildren<Text>().text = "-0,1";
                GameObject.Find("-5A").GetComponentInChildren<Text>().text = "-0,5";
                GameObject.Find("-20A").GetComponentInChildren<Text>().text = "-2";

                GameObject.Find("+1B").GetComponentInChildren<Text>().text = "+0,1";
                GameObject.Find("+5B").GetComponentInChildren<Text>().text = "+0,5";
                GameObject.Find("+20B").GetComponentInChildren<Text>().text = "+2";
                GameObject.Find("-1B").GetComponentInChildren<Text>().text = "-0,1";
                GameObject.Find("-5B").GetComponentInChildren<Text>().text = "-0,5";
                GameObject.Find("-20B").GetComponentInChildren<Text>().text = "-2";

                nextCounter++;
                startCounter = 3;
                increment = 0;

                UpdateCountA();
                UpdateCountB();
            }
        }
        else if (nextCounter == 10)
        {
            if (startCounter >= 0)
            {
                for (int i = 0; i < rows; i++)
                {
                    if((float)startCounter / 10f + ((float)i * (float)increment / 10f) < 0.1f)
                    {
                        levelParameters[i, 10] = 0.1f;
                    }
                    else
                    {
                        levelParameters[i, 10] = (float)startCounter / 10f + ((float)i * (float)increment / 10f);
                    }
                }
            }

            setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

            foreach (GameObject button in setParameters)
            {
                Destroy(button);
            }

            setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

            foreach (GameObject button in setParametersB)
            {
                Destroy(button);
            }

            SaveInFile();
            GoToNeutralPosition();

            for (int j = 0; j < rows; j++)
            {
                Debug.LogError(levelParameters[j, 1] + " " + levelParameters[j, 2] + " " + levelParameters[j, 3] + " " + levelParameters[j, 4] + " " + levelParameters[j, 5] + " " + levelParameters[j, 6] + " " +
                               levelParameters[j, 7] + " " + levelParameters[j, 8] + " " + levelParameters[j, 9] + " " + levelParameters[j, 10]);
            }
        }
        else
        {
            Debug.LogError("nextCounter out of boundaries");
        }
    }

    public void Back()
    {
        if (nextCounter == 1)
        {
            setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

            foreach (GameObject button in setParametersB)
            {
                button.transform.localScale = new Vector3(0, 0, 0);
            }

            setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

            foreach (GameObject button in setParameters)
            {
                button.transform.localScale = new Vector3(1, 1, 1);
            }

            laneButtons = GameObject.FindGameObjectsWithTag("button_lanes");

            foreach (GameObject button in laneButtons)
            {
                button.transform.localScale = new Vector3(0, 0, 0);
            }

            nextCounter--;

            startCounter = 5;
            increment = 0;

            GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the number of levels (1-50)";

            UpdateCountA();            
        }
        else if (nextCounter == 2)
        {
            setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

            foreach (GameObject button in setParameters)
            {
                button.transform.localScale = new Vector3(0, 0, 0);
            }

            setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

            foreach (GameObject button in setParametersB)
            {
                button.transform.localScale = new Vector3(0, 0, 0);
            }

            laneButtons = GameObject.FindGameObjectsWithTag("button_lanes");

            foreach (GameObject button in laneButtons)
            {
                button.transform.localScale = new Vector3(1.10125f, 1.10125f, 1.10125f);
            }

            GameObject.Find("back").transform.localScale = new Vector3(1, 1, 1);

            startCounter = 3;
            increment = 0;

            UpdateCountA();
            UpdateCountB();

            nextCounter--;

            GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the number of lanes (2-4)";

        }
        else if (nextCounter == 3)
        {
            nextCounter--;

            startCounter = 15;
            increment = 5;

            UpdateCountA();
            UpdateCountB();

            GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the movement angle for the left/downward direction";
        }
        else if (nextCounter == 4)
        {
            nextCounter--;

            startCounter = 15;
            increment = 5;

            UpdateCountA();
            UpdateCountB();

            GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the movement angle for the right/upward direction";

            GameObject.Find("+1A").GetComponentInChildren<Text>().text = "+1";
            GameObject.Find("+5A").GetComponentInChildren<Text>().text = "+5";
            GameObject.Find("+20A").GetComponentInChildren<Text>().text = "+20";
            GameObject.Find("-1A").GetComponentInChildren<Text>().text = "-1";
            GameObject.Find("-5A").GetComponentInChildren<Text>().text = "-5";
            GameObject.Find("-20A").GetComponentInChildren<Text>().text = "-20";

            GameObject.Find("+1B").GetComponentInChildren<Text>().text = "+1";
            GameObject.Find("+5B").GetComponentInChildren<Text>().text = "+5";
            GameObject.Find("+20B").GetComponentInChildren<Text>().text = "+20";
            GameObject.Find("-1B").GetComponentInChildren<Text>().text = "-1";
            GameObject.Find("-5B").GetComponentInChildren<Text>().text = "-5";
            GameObject.Find("-20B").GetComponentInChildren<Text>().text = "-20";
        }
        else if (nextCounter == 5)
        {
            nextCounter--;

            GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the movement angle for the neutral position";

            GameObject.Find("+1A").GetComponentInChildren<Text>().text = "+0,1";
            GameObject.Find("+5A").GetComponentInChildren<Text>().text = "+0,5";
            GameObject.Find("+20A").GetComponentInChildren<Text>().text = "+2";
            GameObject.Find("-1A").GetComponentInChildren<Text>().text = "-0,1";
            GameObject.Find("-5A").GetComponentInChildren<Text>().text = "-0,5";
            GameObject.Find("-20A").GetComponentInChildren<Text>().text = "-2";

            GameObject.Find("+1B").GetComponentInChildren<Text>().text = "+0,1";
            GameObject.Find("+5B").GetComponentInChildren<Text>().text = "+0,5";
            GameObject.Find("+20B").GetComponentInChildren<Text>().text = "+2";
            GameObject.Find("-1B").GetComponentInChildren<Text>().text = "-0,1";
            GameObject.Find("-5B").GetComponentInChildren<Text>().text = "-0,5";
            GameObject.Find("-20B").GetComponentInChildren<Text>().text = "-2";

            startCounter = 30;
            increment = 0;

            UpdateCountA();
            UpdateCountB();
        }
        else if (nextCounter == 6)
        {
            nextCounter--;

            GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the speed of the car";

            startCounter = 10;
            increment = 0;

            UpdateCountA();
            UpdateCountB();
        }
        else if (nextCounter == 7)
        {
            nextCounter--;

            GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the distance between obstacles";

            startCounter = 60;
            increment = -1;

            UpdateCountA();
            UpdateCountB();
        }
        else if (nextCounter == 8)
        {
            nextCounter--;

            GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the time for the level";

            GameObject.Find("text_feedback_mode").transform.localScale = new Vector3(0, 0, 0);

            setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

            foreach (GameObject button in setParameters)
            {
                button.transform.localScale = new Vector3(1, 1, 1);
            }

            setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

            foreach (GameObject button in setParametersB)
            {
                button.transform.localScale = new Vector3(1, 1, 1);
            }

            gameModeButtons = GameObject.FindGameObjectsWithTag("button_gamemodes");

            foreach (GameObject button in gameModeButtons)
            {
                button.transform.localScale = new Vector3(0, 0, 0);
            }

            startCounter = 100;
            increment = 0;

            UpdateCountA();
            UpdateCountB();
        }
        else if (nextCounter == 9)
        {
            nextCounter--;

            GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the feedback mode";

            setParametersB = GameObject.FindGameObjectsWithTag("SetParametersB");

            foreach (GameObject button in setParametersB)
            {
                button.transform.localScale = new Vector3(0, 0, 0);
            }

            setParameters = GameObject.FindGameObjectsWithTag("SetParameters");

            foreach (GameObject button in setParameters)
            {
                button.transform.localScale = new Vector3(0, 0, 0);
            }

            GameObject.Find("back").transform.localScale = new Vector3(1, 1, 1);
            //GameObject.Find("text_feedback_mode").transform.localScale = new Vector3(1, 1, 1);

            gameModeButtons = GameObject.FindGameObjectsWithTag("button_gamemodes");

            foreach (GameObject button in gameModeButtons)
            {
                button.transform.localScale = new Vector3(1, 1, 1);
            }

            startCounter = 1;
            increment = 0;

            UpdateCountA();
            UpdateCountB();
        }
        else if (nextCounter == 10)
        {
            nextCounter--;

            GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Set the feedback force in %";

            GameObject.Find("+1A").GetComponentInChildren<Text>().text = "+1";
            GameObject.Find("+5A").GetComponentInChildren<Text>().text = "+5";
            GameObject.Find("+20A").GetComponentInChildren<Text>().text = "+20";
            GameObject.Find("-1A").GetComponentInChildren<Text>().text = "-1";
            GameObject.Find("-5A").GetComponentInChildren<Text>().text = "-5";
            GameObject.Find("-20A").GetComponentInChildren<Text>().text = "-20";

            GameObject.Find("+1B").GetComponentInChildren<Text>().text = "+1";
            GameObject.Find("+5B").GetComponentInChildren<Text>().text = "+5";
            GameObject.Find("+20B").GetComponentInChildren<Text>().text = "+20";
            GameObject.Find("-1B").GetComponentInChildren<Text>().text = "-1";
            GameObject.Find("-5B").GetComponentInChildren<Text>().text = "-5";
            GameObject.Find("-20B").GetComponentInChildren<Text>().text = "-20";

            startCounter = 80;
            increment = -5;

            UpdateCountA();
            UpdateCountB();
        }
        else
        {
            Debug.LogError("next counter out of boundaries");
        }
    }

    public void SaveInFile()
    {
        string output = "Level,Lanes,Movement angle left in deg,Movement angle right in deg,Movement angle neutral in deg,Speed in m/s,Distance between obstacles in m,Time in s,Feedback mode,Force in %,Time delay in s\n";

        for (int i = 0; i < rows; i++)
        {
            output += (i + 1) + ",";

            for (int j = 1; j < 11; j++ )
            {
                output += levelParameters[i,j].ToString("F", CultureInfo.CreateSpecificCulture("en-CA")) + ",";
            }

            output += "\n";
        }

        if (gameMode == 1)
        {
            File.WriteAllText(Application.dataPath + "/SaveFiles/Supination_pronation.csv", output);
        }
        else if (gameMode == 2)
        {
            File.WriteAllText(Application.dataPath + "/SaveFiles/Ulnar and radial deviation.csv", output);
        }
        else if (gameMode == 3)
        {
            File.WriteAllText(Application.dataPath + "/SaveFiles/Flexion and extension.csv", output);
        }
        else
        {
            Debug.LogError("no gamemode selected");
        }

    }

    public void DestroyButtons()
    {
        gameObjects = GameObject.Find("Gamemode_text");

        Destroy(gameObjects);

        buttons = GameObject.FindGameObjectsWithTag("button");

        foreach (GameObject button in buttons)
        {
            Destroy(button);
        }

        loadOrSave = GameObject.FindGameObjectsWithTag("LoadOrSave");

        foreach (GameObject button in loadOrSave)
        {
            button.transform.localScale = new Vector3(1, 1.10125f, 1.10125f);
        }

    }

    public void GoToNeutralPosition()
    {
        GameObject.Find("header").GetComponent<TextMeshProUGUI>().text = "Move your hand into the neutral position and press\nSpace";

        GameObject.Find("TouchXActor (1)").transform.localScale = new Vector3(1, 1, 1);

        if (LevelManagement.gameMode == 1)
        {
            GameObject.Find("Position1").transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

            GameObject.Find("Position1 (1)").transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }
        else if (LevelManagement.gameMode == 2 || LevelManagement.gameMode == 3)
        {
            GameObject.Find("Position2").transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }
        else
        {
            Debug.LogError("no gamemode found");
        }
    }

    public void LoadLastData()
    {
        loadOrSave = GameObject.FindGameObjectsWithTag("LoadOrSave");

        foreach (GameObject button in loadOrSave)
        {
            Destroy(button);
        }

        if (gameMode == 1)                                                       
        {
            fileName = Application.dataPath + "/SaveFiles/Supination_pronation.csv";
        }
        else if (gameMode == 2)
        {
            fileName = Application.dataPath + "/SaveFiles/Ulnar and radial deviation.csv";
        }
        else if (gameMode == 3)
        {
            fileName = Application.dataPath + "/SaveFiles/Flexion and extension.csv";
        }
        else
        {
            Debug.LogError("no gamemode selected");
        }

        SetArray();
        for (int j = 0; j < rows; j++)
        {
            Debug.LogError(levelParameters[j, 1] + " " + levelParameters[j, 2] + " " + levelParameters[j, 3] + " " + levelParameters[j, 4] + " " + levelParameters[j, 5] + " " + levelParameters[j, 6] + " " +
                           levelParameters[j, 7] + " " + levelParameters[j, 8] + " " + levelParameters[j, 9] + " " + levelParameters[j, 10]);
        }
        GoToNeutralPosition();
    }

}
