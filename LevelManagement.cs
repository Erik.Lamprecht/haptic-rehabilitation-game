using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System;
using UnityEngine;
using System.IO;

public static class LevelManagement
{
    public static float [,]     levelParameters;
    private static StreamWriter writer;
    private static string       datapathOutput;
    private static StreamReader reader;

    public static int           gameMode;
    public static string        fileName;
    public static string        gameModeString;

    public static int           rows = 0;
    public static int           columns = 0;

    public static int           currentLevel = 0;
    public static int           levelFirsttime = 0;
    public static float         movementTimer = 0;
    public static string        direction;
    private static float        angle;

    public static float         neutralAngle;
    public static float         laneWidth;

    public static bool          gameHasStarted = false;

    public static bool          firstStart = true;                                          //only for debug mode

    public static int           startCounter = 5;
    public static int           increment = 0;
    public static int           nextCounter = 0;

    public static void SetArraysize()
    {
        levelParameters = new float[50, 11];                                                 //Sets size for level-parameter-array

        datapathOutput = Application.dataPath + "/measured movements.csv";

        writer = new StreamWriter(datapathOutput, true);
        writer.WriteLine();
        writer.WriteLine("New session," + DateTime.Now);
        writer.Close();

        return;
    }

    public static void SetArrayPath()
    {
        if (gameMode == 1)                                                                   //loads the input document for choosen gamemode
        {
            fileName = Application.dataPath + "/Input/Supination_pronation.csv";
        }
        else if (gameMode == 2)
        {
            fileName = Application.dataPath + "/Input/Ulnar and radial deviation.csv";
        }
        else if (gameMode == 3)
        {
            fileName = Application.dataPath + "/Input/Flexion and extension.csv";
        }
        else
        {
            Debug.LogError("no gamemode selected");
        }
    }


    public static void SetArray()                                                           //loads the input document and gets the level parameters
    {
        try
        {
            reader = new StreamReader(File.OpenRead(fileName));                             //opens input document
        }
        catch
        {
            Debug.LogError("input document not found");
        }
        
        _ = reader.ReadLine();                                                              //first line (header of the table) is read but not used

        string line = reader.ReadLine();

        while (string.Equals(line, null) == false && string.Equals(line, ",,,,,,,,,,") == false) //reads lines until the first empty line
        {
            var values = line.Split(',');                                                   //splits line-string into an array of strings for each cell

            for (columns = 0; columns < 11; columns++)
            {
                try
                {
                    float value = float.Parse(values[columns], CultureInfo.CreateSpecificCulture("en-CA"));   //each string is parsed into a float

                    if(columns == 1)
                    {
                        if (value <= 2)
                        {
                            levelParameters[rows, 1] = 2;
                        }
                        else if (value >= 4)
                        {
                            levelParameters[rows, 1] = 4;
                        }
                        else
                        {
                            levelParameters[rows, 1] = 3;
                        }
                    }
                    else if (columns == 2)
                    {
                        if (value < 3)
                        {
                            levelParameters[rows, 2] = 3;
                        }
                        else if (value > 90 && (gameMode == 1 || gameMode == 3))
                        {
                            levelParameters[rows, 2] = 90;
                        }
                        else if (value > 30 && gameMode == 2)
                        {
                            levelParameters[rows, 2] = 30;
                        }
                        else
                        {
                            levelParameters[rows, 2] = value;
                        }
                    }
                    else if (columns == 3)
                    {
                        if (value < 3)
                        {
                            levelParameters[rows, 3] = 3;
                        }
                        else if (value > 90 && (gameMode == 1 || gameMode == 3))
                        {
                            levelParameters[rows, 3] = 90;
                        }
                        else if (value > 30 && gameMode == 2)
                        {
                            levelParameters[rows, 3] = 30;
                        }
                        else
                        {
                            levelParameters[rows, 3] = value;
                        }
                    }
                    else if (columns == 4)
                    {
                        if (value < 0.1f)
                        {
                            levelParameters[rows, 4] = 0.1f;
                        }
                        else if (value > levelParameters[rows, 2] || value > levelParameters[rows, 3])
                        {
                            levelParameters[rows, 4] = Math.Min(levelParameters[rows, 2], levelParameters[rows, 3]);
                        }
                        else
                        {
                            levelParameters[rows, 4] = value;
                        }
                    }
                    else if (columns == 5)
                    {
                        if (value < 1)
                        {
                            levelParameters[rows, 5] = 1;
                        }
                        else
                        {
                            levelParameters[rows, 5] = value;
                        }
                    }
                    else if (columns == 6)
                    {
                        if (value < 5)
                        {
                            levelParameters[rows, 6] = 5;
                        }
                        else
                        {
                            levelParameters[rows, 6] = value;
                        }
                    }
                    else if (columns == 7)
                    {
                        if (value < 1)
                        {
                            levelParameters[rows, 7] = 1;
                        }
                        else
                        {
                            levelParameters[rows, 7] = value;
                        }
                    }
                    else if (columns == 8)
                    {
                        if (value <= 1)
                        {
                            levelParameters[rows, 8] = 1;
                        }
                        else if (value >= 5)
                        {
                            levelParameters[rows, 8] = 5;
                        }
                        else
                        {
                            levelParameters[rows, 8] = value;
                        }
                    }
                    else if (columns == 9)
                    {
                        if (value <= 0)
                        {
                            levelParameters[rows, 9] = 0;
                        }
                        else if (value >= 100)
                        {
                            levelParameters[rows, 9] = 100;
                        }
                        else
                        {
                            levelParameters[rows, 9] = value;
                        }
                    }
                    else if (columns == 10)
                    {
                        if (value < 0.1f)
                        {
                            levelParameters[rows, 10] = 0.1f;
                        }
                        else
                        {
                            levelParameters[rows, 10] = value;
                        }
                    }
                    levelParameters[rows, columns] = value;                                     //the level-parameter array is filled with the values
                }
                catch
                {
                    if (columns == 1)
                    {
                        levelParameters[rows, 1] = 2;                      
                    }
                    else if (columns == 2)
                    {
                        levelParameters[rows, 2] = 5;
                    }
                    else if (columns == 3)
                    {
                        levelParameters[rows, 3] = 5;
                    }
                    else if (columns == 4)
                    {
                        levelParameters[rows, 4] = 3;
                    }
                    else if (columns == 5)
                    {
                        levelParameters[rows, 5] = 10;
                    }
                    else if (columns == 6)
                    {
                        levelParameters[rows, 6] = 60;
                    }
                    else if (columns == 7)
                    {
                        levelParameters[rows, 7] = 60;
                    }
                    else if (columns == 8)
                    {
                        levelParameters[rows, 8] = 3;
                    }
                    else if (columns == 9)
                    {
                        levelParameters[rows, 9] = 0;
                    }
                    else if (columns == 10)
                    {
                        levelParameters[rows, 10] = 0.3f;
                    }
                    else
                    {
                        Debug.LogError("colums out of boundaries");
                    }

                    Debug.LogError("value in " + fileName + " in level " + (rows + 1) + " colum " + (columns + 1) + " must be a number");
                }
            }

            rows++;
            line = reader.ReadLine();
        }

        return;
    }


    public static void LogMovement()
    {
        writer = new StreamWriter(datapathOutput, true);
        if (String.Equals("left",direction))
        {
            angle = levelParameters[currentLevel, 2] - levelParameters[currentLevel, 4];
        }
        else
        {
            angle = levelParameters[currentLevel, 3] - levelParameters[currentLevel, 4];
        }

        writer.WriteLine(direction + "," + angle.ToString("F", CultureInfo.CreateSpecificCulture("en-CA"))
                           + "," + movementTimer.ToString("F", CultureInfo.CreateSpecificCulture("en-CA"))
                   + "," + (angle/movementTimer).ToString("F", CultureInfo.CreateSpecificCulture("en-CA")));

        writer.Close();
    }

    public static void WriteHeader()
    {
        writer = new StreamWriter(datapathOutput, true);
        writer.WriteLine();
        writer.WriteLine("Gamemode: " + gameModeString + ",Level: " + (currentLevel+1));
        writer.WriteLine();
        writer.WriteLine("Direction,Angle in deg,Time in s,average angular speed in deg/s");
        writer.Close();
    }

    public static void LogReset()
    {
        writer = new StreamWriter(datapathOutput, true);
        writer.WriteLine("reset,reset,reset,reset");
        writer.Close();
    }

    public static void LogFinished()
    {
        writer = new StreamWriter(datapathOutput, true);
        writer.WriteLine("game finished,game finished,game finished,game finished");
        writer.Close();
    }

}