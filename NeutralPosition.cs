using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static LevelManagement;
using TMPro;

public class NeutralPosition : MonoBehaviour
{
    public GameObject z_object;
    public GameObject y_object;
    private double[] jointAngles = new double[3];
    private double[] gimbalAngles = new double[3];

    void Start()
    {
        LevelManagement.SetArraysize();
    }

    // Update is called once per frame
    void Update()
    {
        HapticPlugin.getJointAngles("Default Device", jointAngles, gimbalAngles);
        
        if(Input.GetButtonDown("Jump"))
        {
            if (LevelManagement.gameMode == 1)
            {
                if (gimbalAngles[1] * Mathf.Rad2Deg > 0 && Mathf.Abs((float)gimbalAngles[0]) * Mathf.Rad2Deg < 30)
                {
                    Vector3 position = z_object.transform.position;

                    LevelManagement.neutralAngle = Mathf.Atan2(position.x, position.y) * Mathf.Rad2Deg;

                    GameObject.Find("text_next_level").GetComponent<TextMeshProUGUI>().text = "Press Enter to start the Game";
                }
                else
                {
                    GameObject.Find("text_next_level").GetComponent<TextMeshProUGUI>().text = "Hand is not in neutral position!";
                }
            }
            else if (LevelManagement.gameMode == 2)
            {
                if (gimbalAngles[0] * Mathf.Rad2Deg > 45 && Mathf.Abs((float)gimbalAngles[1]) * Mathf.Rad2Deg < 30)
                {
                    Vector3 position = z_object.transform.position;

                    LevelManagement.neutralAngle = Mathf.Atan2(position.x, position.z) * Mathf.Rad2Deg;

                    GameObject.Find("text_next_level").GetComponent<TextMeshProUGUI>().text = "Press Enter to start the Game";
                }
                else
                {
                    GameObject.Find("text_next_level").GetComponent<TextMeshProUGUI>().text = "Hand is not in neutral position!";
                }
            }
            else if (LevelManagement.gameMode == 3)
            {
                if (gimbalAngles[0] * Mathf.Rad2Deg > 45 && Mathf.Abs((float)gimbalAngles[1]) * Mathf.Rad2Deg < 30)
                {
                    Vector3 position = z_object.transform.position;

                    LevelManagement.neutralAngle = Mathf.Atan2(position.x, position.z) * Mathf.Rad2Deg;

                    GameObject.Find("text_next_level").GetComponent<TextMeshProUGUI>().text = "Press Enter to start the Game";
                }
                else
                {
                    GameObject.Find("text_next_level").GetComponent<TextMeshProUGUI>().text = "Hand is not in neutral position!";
                }
            }
            else
            {
                Debug.LogError("No gamemode found");
            }
        }

        if (Input.GetButtonDown("Submit") && LevelManagement.gameMode >= 1 && LevelManagement.gameMode <= 3)
        {
            LevelManagement.WriteHeader();
            SceneManager.LoadScene("Game");
        }
    }
}
