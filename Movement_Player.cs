using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class Movement_Player : MonoBehaviour
{
    //variable of the camera object
    private GameObject gameCamera;

    //variables to spawn the road
    private Vector3 scaleOfRoadSection;
    private Quaternion worldRotation;
    private readonly int numberOfRoadSections = 15;
    private GameObject road_section;
    private Vector3 positionOfRoadSection;

    //variables to spawn trees
    private GameObject[] trees;

    //variables of to spawn the other cars/trucks/busses
    private GameObject[] obstacles;
    private GameObject[] roads;
    private float positionLastRoad;
    private int numberOfObstacles;
    private int numberOfLanes;
    private float minimalDistanceBetweenObstacles;
    private float variationDistanceBetweenObstacles;
    private float position;
    private int randomLane;
    private int lastRandomeLane;
    private bool sameLane = false;
    private float randomDistanceBetweenObstacles;
    private Vector3 positionOfCar;
    private GameObject randomCarObject;
    private MeshFilter meshFilter;
    private string meshName;
    private float heightOfCar;

    //variables to calculate the player inputs
    public GameObject z_object;
    public GameObject y_object;
    public float angle;
    public float angleDifference;
    public float currentAngle;

    //variables to conduct the lane change
    private bool movementCanStart;
    private bool laneChangeLeft = false;
    private bool laneChangeRight = false;
    private float target;
    private readonly float speedLaneChange = 4f;
    private float thresholdMovementCanStart;
    private float thresholdMovementCompleteLeft;
    private float thresholdMovementCompleteRight;

    //variables for level management, time and UI
    private float countdown = 4f;
    private float levelTimer;
    private bool levelFinished = false;
    private GameObject countdownText;
    private GameObject currentVectorObject;
    private GameObject vectorLeft;
    private GameObject vectorRight;
    private GameObject imageCurrentVector;
    private GameObject timerText;
    private GameObject levelInformation;
    private bool gameFinished = false;

    //variables for sound
    public AudioClip indicator;
    public AudioClip crash;
    public AudioClip win;
    private bool audioOn = false;

    //variables for force feedback
    private float vibrationCountdown = 0.8f;
    private bool vibration = true;
    double[] vibrationDirection = new double[3];
    private double[] direction = new double[3];
    private int feedbackMode;
    private bool firstMove = true;
    private bool feedback = false;
    private bool countdownForceFeedback = false;
    private bool feedbackPossible = false;
    private bool firstMoveMovementSupport = true;
    private bool feedbackMovementSupport = false;
    private float feedbackForce = 0.2f;
    private bool disableFeedback = false;
    private float feedbackCounter = 0.5f;
    private float feedbackCounterReset = 0.5f;
    private float feedbackCounterIncrement = 0.05f;

    //variables for movement support
    private bool laneChanged = true;
    private int freeLane;
    private bool wasInNeutralPosition = true;
    private float countdownLaneChange;
    private int currentLane = 1;
    private float countdownLaneChangeReset;

    //debug mode: test mode to run the game while no haptic device is connected (input by left/right key)
    private readonly bool debug = false; //haptic device has to be disabled in unity editor first


    // Start is called before the first frame update
    void Start()
    {
        gameCamera = GameObject.Find("Main Camera");                                            //Finds camera object

        LevelManagement.gameHasStarted = false;

        if (debug == false)                                                                     //Is disabled in debug mode (when no haptic device connected)
        {

        }
        else                                                                                    //only in debug mode (when no haptic device connected)
        {
            if (LevelManagement.firstStart == true)
            {
                LevelManagement.firstStart = false;
                LevelManagement.gameMode = 1;
                LevelManagement.SetArraysize();
                LevelManagement.SetArray();
            }
        }

        // Gets level settings from static LevelManagement class

        numberOfLanes = (int)LevelManagement.levelParameters[LevelManagement.currentLevel, 1];
        thresholdMovementCompleteLeft = LevelManagement.levelParameters[LevelManagement.currentLevel, 2];
        thresholdMovementCompleteRight = LevelManagement.levelParameters[LevelManagement.currentLevel, 3];
        thresholdMovementCanStart = LevelManagement.levelParameters[LevelManagement.currentLevel, 4];
        minimalDistanceBetweenObstacles = LevelManagement.levelParameters[LevelManagement.currentLevel, 6];
        levelTimer = LevelManagement.levelParameters[LevelManagement.currentLevel, 7];
        feedbackMode = (int)LevelManagement.levelParameters[LevelManagement.currentLevel, 8];
        feedbackForce = LevelManagement.levelParameters[LevelManagement.currentLevel, 9] / 100 * 0.5f;
        countdownLaneChangeReset = LevelManagement.levelParameters[LevelManagement.currentLevel, 10];

        variationDistanceBetweenObstacles = minimalDistanceBetweenObstacles / 10;

        //Finds the UI elements for graphic feedback

        //UI element that displayes the countdown and game instructions
        countdownText = GameObject.Find("Text (TMP)");

        //UI element that shows the current angle in relation to the neutral position and the angle that has to be reached to complete a lane switch
        currentVectorObject = GameObject.Find("CurrentVector");
        imageCurrentVector = GameObject.Find("ImageCurrentVector");                         //image component that changes color if movement is complete

        //UI element that shows the timer (how long the patient has to play to comlete the level)
        timerText = GameObject.Find("TimerText");

        //UI element that shows the gamemode (type of required movement) and the current level
        levelInformation = GameObject.Find("LevelInformation");

        //UI elements that show the angle that has to be reached to complete a movement and generate a lane switch
        vectorLeft = GameObject.Find("VectorLeft");
        vectorLeft.transform.rotation = Quaternion.Euler(0, 0, thresholdMovementCompleteLeft);      //sets angle for the UI elements that shows the angle
        vectorRight = GameObject.Find("VectorRight");                                               //that has to be reached to complete a lane switch
        vectorRight.transform.rotation = Quaternion.Euler(0, 0, -thresholdMovementCompleteRight);


        if (LevelManagement.gameMode == 1)                                                      //road width is scaled depending on the gamemode 
        {                                                                                       //and required movement ranges
            if (thresholdMovementCompleteLeft >= 60 || thresholdMovementCompleteRight >= 60)
            {
                scaleOfRoadSection.Set(1.2f, 1, 1);
                LevelManagement.laneWidth = 1.2f * 3.2f;
            }
            else if (thresholdMovementCompleteLeft >= 45 || thresholdMovementCompleteRight >= 45)
            {
                scaleOfRoadSection.Set(1.1f, 1, 1);
                LevelManagement.laneWidth = 1.1f * 3.2f;
            }
            else
            {
                scaleOfRoadSection.Set(1, 1, 1);
                LevelManagement.laneWidth = 3.2f;
            }
        }
        else if (LevelManagement.gameMode == 2)
        {
            if (thresholdMovementCompleteLeft >= 20 || thresholdMovementCompleteRight >= 20)
            {
                scaleOfRoadSection.Set(1.2f, 1, 1);
                LevelManagement.laneWidth = 1.2f * 3.2f;
            }
            else if (thresholdMovementCompleteLeft >= 15 || thresholdMovementCompleteRight >= 15)
            {
                scaleOfRoadSection.Set(1.1f, 1, 1);
                LevelManagement.laneWidth = 1.1f * 3.2f;
            }
            else
            {
                scaleOfRoadSection.Set(1, 1, 1);
                LevelManagement.laneWidth = 3.2f;
            }
        }
        else if (LevelManagement.gameMode == 3)
        {
            if (thresholdMovementCompleteLeft >= 50 || thresholdMovementCompleteRight >= 50)
            {
                scaleOfRoadSection.Set(1.2f, 1, 1);
                LevelManagement.laneWidth = 1.2f * 3.2f;
            }
            else if (thresholdMovementCompleteLeft >= 30 || thresholdMovementCompleteRight >= 30)
            {
                scaleOfRoadSection.Set(1.1f, 1, 1);
                LevelManagement.laneWidth = 1.1f * 3.2f;
            }
            else
            {
                scaleOfRoadSection.Set(1, 1, 1);
                LevelManagement.laneWidth = 3.2f;
            }
        }
        else
        {
            Debug.LogError("No gamemode found");
        }

        if (numberOfLanes <= 2)                                                                 //Puts cumera in the middle of the road 
        {                                                                                       //(position depends on numer of lanes and laneWidth)
            road_section = Resources.Load<GameObject>("road-sections/2_lane_road-section");

            gameCamera.transform.Translate(-(float)LevelManagement.laneWidth * 0.5f, 0f, 0f);
        }
        else if (numberOfLanes == 3)
        {
            road_section = Resources.Load<GameObject>("road-sections/3_lane_road-section");

            gameCamera.transform.Translate(-(float)LevelManagement.laneWidth, 0f, 0f);
        }
        else if (numberOfLanes >= 4)
        {
            road_section = Resources.Load<GameObject>("road-sections/4_lane_road-section");

            gameCamera.transform.Translate(-(float)LevelManagement.laneWidth * 1.5f, 0f, 0f);
        }
        else
        {
            Debug.LogError("Number of lanes not found");
        }

        if ((LevelManagement.levelFirsttime - 1) < LevelManagement.currentLevel)
        {
            if (LevelManagement.levelFirsttime != 0)
            {
                gameCamera.GetComponent<AudioSource>().PlayOneShot(win);
            }
            LevelManagement.levelFirsttime++;
        }
        else
        {
            gameCamera.GetComponent<AudioSource>().PlayOneShot(crash);

            vibrationDirection[0] = 1;
            vibrationDirection[1] = 1;
            vibrationDirection[2] = 1;

            HapticPlugin.setVibrationValues("Default Device", vibrationDirection, 0.2f, 200, 1);
        }

        road_section.transform.localScale = scaleOfRoadSection;

        worldRotation = Quaternion.Euler(0, 0, 0);

        trees = Resources.LoadAll<GameObject>("trees");

        for (int i = 0; i < numberOfRoadSections; i++)                                              //Spawns the road
        {
            positionOfRoadSection.Set(0, 0, i * 30);

            var currentRoadObject = Instantiate(road_section, positionOfRoadSection, worldRotation);

            for (float z = -2.5f; z <= 22.5; z += 10)                                               //Spawns trees
            {
                for (float x = -((float)LevelManagement.laneWidth * (numberOfLanes + 2)); x >= -130; x -= 10)//left side of the road
                {
                    int chance = Random.Range(1, 101);

                    if (chance < 80)                                                                //chance of 80% for a spawn so there are spots with no trees 
                    {                                                                               //which makes it look more naturale
                        int randomTree = Random.Range(0, 4);                                        //tree model is choosen randomly

                        GameObject tree = trees[randomTree];

                        int randomRotationY = Random.Range(0, 360);                                 //rotation is choosen randomly

                        Quaternion randomRotation = Quaternion.Euler(0, randomRotationY, 0);

                        float offsetX = Random.Range(-2f, 2f);                                      //offsets are choosen randomly so it doesn't look
                        float offsetZ = Random.Range(-2f, 2f);                                      //like the trees are on a grid

                        Vector3 treeVector = new(x + offsetX, 0, z + offsetZ);

                        treeVector += currentRoadObject.transform.position;

                        var currentTree = Instantiate(tree, treeVector, randomRotation, currentRoadObject.transform);   //tree is spawned

                        float randomScale = Random.Range(0.3f, 0.8f);                               //scale is choosen randomly so there are different hights

                        Vector3 treeScale = new(randomScale, randomScale, randomScale);

                        currentTree.transform.localScale = treeScale;
                    }
                }

                for (float x = ((float)LevelManagement.laneWidth * 3); x <= 130; x += 10)                  //right side of the road
                {
                    int chance = Random.Range(1, 101);

                    if (chance < 80)                                                                //chance of 80% for a spawn so there are spots with no trees
                    {                                                                               //which makes it look more naturale
                        int randomTree = Random.Range(0, 4);                                        //tree model is choosen randomly

                        GameObject tree = trees[randomTree];

                        int randomRotationY = Random.Range(0, 360);                                 //rotation is choosen randomly

                        Quaternion randomRotation = Quaternion.Euler(0, randomRotationY, 0);

                        float offsetX = Random.Range(-2f, 2f);                                      //offsets are choosen randomly so it doesn't look
                        float offsetZ = Random.Range(-2f, 2f);                                      //like the trees are on a grid

                        Vector3 treeVector = new(x + offsetX, 0, z + offsetZ);

                        treeVector += currentRoadObject.transform.position;

                        var currentTree = Instantiate(tree, treeVector, randomRotation, currentRoadObject.transform);   //tree is spawned

                        float randomScale = Random.Range(0.3f, 0.8f);                               //scale is choosen randomly so there are different hights

                        Vector3 treeScale = new(randomScale, randomScale, randomScale);

                        currentTree.transform.localScale = treeScale;
                    }
                }
            }
        }


        // obstacles are spawned

        obstacles = Resources.LoadAll<GameObject>("obstacles");                             //Finds all Prefabs in Assets/Resources/Prefabs

        numberOfObstacles = obstacles.Length;

        roads = GameObject.FindGameObjectsWithTag("road");                                  //Finds all road objects


        foreach (GameObject road in roads)
        {
            if (road.transform.position.z > positionLastRoad)                               //finds last road object
            {
                positionLastRoad = road.transform.position.z;                               //finds position of last road object
            }
        }

        position = minimalDistanceBetweenObstacles;                                         //Position of first obstacle

        while (position <= positionLastRoad)
        {
            randomLane = Random.Range(0, numberOfLanes);                                    //chooses which lane stays free

            while (randomLane == lastRandomeLane && sameLane == true)                       //makes sure the same lane isn't coosen more than 2
            {                                                                               //times in a row
                randomLane = Random.Range(0, numberOfLanes);
            }

            for (int i = 0; i < numberOfLanes; i++)
            {
                if (i != randomLane)
                {
                    randomCarObject = obstacles[Random.Range(0, numberOfObstacles)];        //chooses random car                      

                    meshFilter = randomCarObject.GetComponent<MeshFilter>();

                    meshName = meshFilter.sharedMesh.name;

                    if (string.Equals(meshName, "Bus"))                                     //adjusts hight for different cars/trucks/busses
                        heightOfCar = 1.627174f;
                    else if (string.Equals(meshName, "Truck 1"))
                        heightOfCar = 1.983313f;
                    else if (string.Equals(meshName, "Truck 2"))
                        heightOfCar = 1.766092f;
                    else if (string.Equals(meshName, "Car 1"))
                        heightOfCar = 0.8485862f;
                    else if (string.Equals(meshName, "Car 2"))
                        heightOfCar = 0.7575706f;
                    else if (string.Equals(meshName, "Car 3"))
                        heightOfCar = 0.7864221f;
                    else if (string.Equals(meshName, "Car 4"))
                        heightOfCar = 1.262459f;
                    else if (string.Equals(meshName, "Car 5"))
                        heightOfCar = 0.981249f;
                    else if (string.Equals(meshName, "Car 6"))
                        heightOfCar = 0.8591423f;

                    randomDistanceBetweenObstacles = Random.Range(0, variationDistanceBetweenObstacles);    //chooses distance variation

                    //sets y,y,z value of the position to spawn the car
                    positionOfCar.Set(-(float)LevelManagement.laneWidth * i, heightOfCar, position + randomDistanceBetweenObstacles);

                    Instantiate(randomCarObject, positionOfCar, gameObject.transform.rotation);             //spawns car

                }

            }

            position += minimalDistanceBetweenObstacles;

            if (randomLane == lastRandomeLane)                                                               //checks if same lane was free twice in a row
            {
                sameLane = true;
            }
            else
            {
                sameLane = false;
                lastRandomeLane = randomLane;
            }

        }

    }

    void GetAngle()
    {
        if (debug == false)                                                                     //Is disabled in debug mode (when no haptic device connected)
        {
            if (LevelManagement.gameMode == 1)
            {
                Vector3 position = z_object.transform.position;

                currentAngle = Mathf.Atan2(position.x, position.y) * Mathf.Rad2Deg;
            }
            else if (LevelManagement.gameMode == 2)
            {
                Vector3 position = z_object.transform.position;

                currentAngle = Mathf.Atan2(position.x, position.z) * Mathf.Rad2Deg;
            }
            else if (LevelManagement.gameMode == 3)
            {
                Vector3 position = y_object.transform.position;

                currentAngle = Mathf.Atan2(position.y, position.z) * Mathf.Rad2Deg;
            }
            else
            {
                Debug.LogError("No gamemode found");
            }

        }
        else                                                                                    //only in debug mode (when no haptic device connected)
        {
            if (laneChangeLeft == false && laneChangeRight == false && movementCanStart == false)
            {
                movementCanStart = true;
                LevelManagement.movementTimer = 0;
            }

            if (Input.GetButton("left") && movementCanStart == true)
            {
                laneChangeLeft = true;
                movementCanStart = false;
                target -= (float)LevelManagement.laneWidth;
                LevelManagement.direction = "left";
                LevelManagement.LogMovement();
            }

            if (Input.GetButton("right") && movementCanStart == true)
            {
                laneChangeRight = true;
                movementCanStart = false;
                target += (float)LevelManagement.laneWidth;
                LevelManagement.direction = "right";
                LevelManagement.LogMovement();
            }
        }

        angle = Mathf.DeltaAngle(LevelManagement.neutralAngle, currentAngle);                   //angle between neutralPosition and current position

        angleDifference = Mathf.Abs(angle);                                                     //absolute value of angle

        currentVectorObject.transform.rotation = Quaternion.Euler(0, 0, -angle);                //sets the current rotation for the UI vector
    }

    void Game()
    {
        GetAngle();

        if (angleDifference < thresholdMovementCanStart && debug == false)                      //detects if stylus is in neutral position(0�)
        {
            movementCanStart = true;
            LevelManagement.movementTimer = 0;
        }

        //detects if stylus was in neutral position after the completion of the last movement
        //and if the current motion is completed

        if (angleDifference > thresholdMovementCompleteLeft && angle < 0 && movementCanStart == true)
        {
            laneChangeLeft = true;
            movementCanStart = false;
            target -= (float)LevelManagement.laneWidth;
            LevelManagement.direction = "left";
            LevelManagement.LogMovement();
            gameCamera.GetComponent<AudioSource>().PlayOneShot(indicator, 1);
            currentLane++;
        }

        if (angleDifference > thresholdMovementCompleteRight && angle > 0 && movementCanStart == true)
        {
            laneChangeRight = true;
            movementCanStart = false;
            target += (float)LevelManagement.laneWidth;
            LevelManagement.direction = "right";
            LevelManagement.LogMovement();
            gameCamera.GetComponent<AudioSource>().PlayOneShot(indicator, 1);
            currentLane--;
        }

        if (laneChangeLeft == true)                                                             //lane change to the left
        {
            gameObject.transform.Translate(-speedLaneChange * Time.deltaTime, 0f, 0f, Space.World);

            if (gameObject.transform.position.x <= target)
            {
                laneChangeLeft = false;
            }
        }

        if (laneChangeRight == true)                                                            //lane change to the right
        {
            gameObject.transform.Translate(speedLaneChange * Time.deltaTime, 0f, 0f, Space.World);

            if (gameObject.transform.position.x >= target)
            {
                laneChangeRight = false;
            }
        }

        if (movementCanStart == true)                                                           //color change if next movement possebile
        {
            imageCurrentVector.GetComponent<Image>().color = Color.green;
        }
        else
        {
            imageCurrentVector.GetComponent<Image>().color = Color.yellow;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.GetButtonDown("Cancel"))
        {
            HapticPlugin.setConstantForceValues("Default Device", direction, 0);
            LevelManagement.gameHasStarted = false;

            #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #endif
            Application.Quit();

        }


        if (LevelManagement.gameHasStarted == true && levelFinished == false)                       //is true while the game is played 
        {
            Game();

            if (feedbackMode == 1)
            {
                MovementSupport();
            }
            else if (feedbackMode == 2)
            {
                ForceFeedbackFromMovementCompleteToNeutralPosition();
            }
            else if (feedbackMode == 3)
            {

            }
            else if (feedbackMode == 4)
            {
                ForceFeedbackToMovementComplete();
            }
            else if (feedbackMode == 5)
            {
                Resistance();
            }
            else
            {
                Debug.LogError("No feedback mode found");
            }


            if (audioOn == false)
            {
                gameCamera.GetComponent<AudioSource>().Play();
                audioOn = true;
            }


            levelTimer -= Time.deltaTime;                                                           //timer counts down

            LevelManagement.movementTimer += Time.deltaTime;                                        //timer for current movement counts up

            float levelTimerRounded = (float)Mathf.Round(levelTimer * 100f) / 100f;

            timerText.GetComponent<TextMeshProUGUI>().text = levelTimerRounded + " s";              //rounded timer is displayed in the UI

            if (levelTimer <= 0)                                                                    //checks if level is finished
            {
                levelFinished = true;
            }
        }
        else if (LevelManagement.gameHasStarted == false && levelFinished == false)                 //is true before the game has started
        {
            GetAngle();                                                                             //checks if patient is in neutral position

            // Gamemode, current level und timer are displayed in the UI
            levelInformation.GetComponent<TextMeshProUGUI>().text = "Gamemode: " + LevelManagement.gameModeString + "\nLevel: " + (LevelManagement.currentLevel + 1);

            float levelTimerRounded = (float)Mathf.Round(levelTimer * 100f) / 100f;
            timerText.GetComponent<TextMeshProUGUI>().text = levelTimerRounded + " s";

            if (angleDifference < thresholdMovementCanStart)                                        //detects if stylus is in neutral position(0�)
            {

                countdown -= Time.deltaTime;                                                        //counts down the time

                if (countdown <= 0.6)
                {
                    LevelManagement.gameHasStarted = true;                                          //if patient is in the neutral position for 3 seconds:
                    countdownText.GetComponent<TextMeshProUGUI>().text = "";                        //game starts
                }

                countdownText.GetComponent<TextMeshProUGUI>().fontSize = 80;

                switch (countdown)                                                                  //if patient stays in the neutral position:
                {                                                                                   //countdown from 3 to 1
                    case > 3:
                        countdownText.GetComponent<TextMeshProUGUI>().text = "3";
                        break;

                    case > 2:
                        countdownText.GetComponent<TextMeshProUGUI>().text = "2";
                        break;

                    case > 1:
                        countdownText.GetComponent<TextMeshProUGUI>().text = "1";
                        break;
                }
            }
            else                                                                                    //if patient leaves the neutral position:
            {                                                                                       //countdown resets
                countdown = 4;
                countdownText.GetComponent<TextMeshProUGUI>().fontSize = 36;
                countdownText.GetComponent<TextMeshProUGUI>().text = "Get into the neutral position to start the next level.";

                timerText.GetComponent<TextMeshProUGUI>().text = "";
                levelInformation.GetComponent<TextMeshProUGUI>().text = "";
            }
        }
        else if (levelFinished == true && gameFinished == false)                                    //is true after the level is finished
        {
            if (disableFeedback == false)                                                           //removes haptic effects
            {
                HapticPlugin.setConstantForceValues("Default Device", direction, 0);
                disableFeedback = true;
            }

            gameCamera.GetComponent<AudioSource>().Pause();                                         //stops engine sounds

            LevelManagement.gameHasStarted = false;

            timerText.GetComponent<TextMeshProUGUI>().text = "";
            levelInformation.GetComponent<TextMeshProUGUI>().text = "";

            countdownText.GetComponent<TextMeshProUGUI>().fontSize = 36;
            countdownText.GetComponent<TextMeshProUGUI>().text = "Level completed!\nPress space to continue to the next level.";

            if (Input.GetButtonDown("Jump"))
            {
                LevelManagement.currentLevel++;

                if (LevelManagement.currentLevel < LevelManagement.rows)                            //detects if last level is finished
                {
                    LevelManagement.WriteHeader();
                    SceneManager.LoadScene("Game");                                                 //next level is loaded
                }
                else
                {
                    countdownText.GetComponent<TextMeshProUGUI>().text = "Congratulations!\nYou finished the game.\nPress space to exit.";
                    gameCamera.GetComponent<AudioSource>().PlayOneShot(win);
                    LevelManagement.LogFinished();
                    gameFinished = true;
                }
            }
        }
        else                                                                                        //is true when the hole game is finished
        {
            if (Input.GetButtonDown("Jump") && gameFinished == true)                                //ends the game
            {
                #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
                #endif
                Application.Quit();
            }
        }

        if(vibration == true)
        {
            if(vibrationCountdown > 0f)
            {
                vibrationCountdown -= Time.deltaTime;
            }
            else
            {
                vibration = false;
                HapticPlugin.setVibrationValues("Default Device", vibrationDirection, 0, 200, 1);
            }
        }

    }

    void OnTriggerEnter(Collider other)                                                         //Collision detection
    {
        LevelManagement.gameHasStarted = false;

        LevelManagement.LogReset();

        SceneManager.LoadScene("Game");                                                         //restarts the level after collision
    }

    void ForceFeedbackToMovementComplete()
    {
        if (angleDifference > (thresholdMovementCompleteLeft + 30) && angle < 0 || angleDifference > (thresholdMovementCompleteRight + 30) && angle > 0)
        {
            HapticPlugin.setConstantForceValues("Default Device", direction, 0);
        }
        else
        {
            if (LevelManagement.gameMode == 1)
            {
                if (angleDifference <= thresholdMovementCompleteLeft && angleDifference <= thresholdMovementCompleteRight && firstMove == false)
                {
                    HapticPlugin.setConstantForceValues("Default Device", direction, 0);

                    firstMove = true;
                }

                if (angleDifference > thresholdMovementCompleteLeft && angle < 0 && firstMove == true)
                {
                    direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                    direction[2] = 0.0;

                    HapticPlugin.setConstantForceValues("Default Device",direction,0.02);

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCompleteLeft && angle < 0 && firstMove == false)
                {
                    direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                    direction[2] = 0.0;

                    double force = 0.04 * (angleDifference - thresholdMovementCompleteLeft) + 0.02;

                    HapticPlugin.setConstantForceValues("Default Device", direction , force);
                }

                if (angleDifference > thresholdMovementCompleteRight && angle > 0 && firstMove == true)
                {
                    direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                    direction[2] = 0.0;

                    HapticPlugin.setConstantForceValues("Default Device", direction, 0.02);

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCompleteRight && angle > 0 && firstMove == false)
                {
                    direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                    direction[2] = 0.0;

                    double force = 0.04 * (angleDifference - thresholdMovementCompleteLeft) + 0.02;

                    HapticPlugin.setConstantForceValues("Default Device", direction, force);
                }
            }
            else if (LevelManagement.gameMode == 2)
            {
                if (angleDifference <= thresholdMovementCompleteLeft && angleDifference <= thresholdMovementCompleteRight && firstMove == false)
                {
                    HapticPlugin.setConstantForceValues("Default Device", direction, 0);

                    firstMove = true;
                }

                if (angleDifference > thresholdMovementCompleteLeft && angle < 0 && firstMove == true)
                {
                    direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = 0.0;
                    direction[2] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, 0.02);

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCompleteLeft && angle < 0 && firstMove == false)
                {
                    direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = 0.0;
                    direction[2] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                    double force = 0.04 * (angleDifference - thresholdMovementCompleteLeft) + 0.02;

                    HapticPlugin.setConstantForceValues("Default Device", direction, force);
                }

                if (angleDifference > thresholdMovementCompleteRight && angle > 0 && firstMove == true)
                {
                    direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = 0.0;
                    direction[2] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, 0.02);

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCompleteRight && angle > 0 && firstMove == false)
                {
                    direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = 0.0;
                    direction[2] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                    double force = 0.04 * (angleDifference - thresholdMovementCompleteLeft) + 0.02;

                    HapticPlugin.setConstantForceValues("Default Device", direction, force);

                }
            }
            else if (LevelManagement.gameMode == 3)
            {
                if (angleDifference <= thresholdMovementCompleteLeft && angleDifference <= thresholdMovementCompleteRight && firstMove == false)
                {
                    HapticPlugin.setConstantForceValues("Default Device", direction, 0);

                    firstMove = true;
                }

                if (angleDifference > thresholdMovementCompleteLeft && angle < 0 && firstMove == true)
                {
                    direction[0] = 0.0;
                    direction[1] = Mathf.Cos(angle * Mathf.Deg2Rad);
                    direction[2] = Mathf.Sin(angle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, 0.02);

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCompleteLeft && angle < 0 && firstMove == false)
                {
                    direction[0] = 0.0;
                    direction[1] = Mathf.Cos(angle * Mathf.Deg2Rad);
                    direction[2] = Mathf.Sin(angle * Mathf.Deg2Rad);

                    double force = 0.04 * (angleDifference - thresholdMovementCompleteLeft) + 0.02;

                    HapticPlugin.setConstantForceValues("Default Device", direction, force);
                }

                if (angleDifference > thresholdMovementCompleteRight && angle > 0 && firstMove == true)
                {
                    direction[0] = 0.0;
                    direction[1] = -Mathf.Cos(angle * Mathf.Deg2Rad);
                    direction[2] = -Mathf.Sin(angle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, 0.02);

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCompleteRight && angle > 0 && firstMove == false)
                {
                    direction[0] = 0.0;
                    direction[1] = -Mathf.Cos(angle * Mathf.Deg2Rad);
                    direction[2] = -Mathf.Sin(angle * Mathf.Deg2Rad);

                    double force = 0.04 * (angleDifference - thresholdMovementCompleteLeft) + 0.02;

                    HapticPlugin.setConstantForceValues("Default Device", direction, force);
                }
            }
        }
    }

    void ForceFeedbackFromMovementCompleteToNeutralPosition()
    {
        if (angleDifference > (thresholdMovementCompleteLeft + 25) && angle < 0 || angleDifference > (thresholdMovementCompleteRight + 25) && angle > 0)
        {
            HapticPlugin.setConstantForceValues("Default Device", direction, 0);

            feedbackCounter = feedbackCounterReset;
        }
        else
        {
            if (angleDifference <= thresholdMovementCanStart && firstMove == false)
            {
                HapticPlugin.setConstantForceValues("Default Device", direction, 0);

                feedbackCounter = feedbackCounterReset;

                firstMove = true;
                feedback = false;
            }

            if (angleDifference > thresholdMovementCompleteLeft && angle < 0 || angleDifference > thresholdMovementCompleteRight && angle > 0)
            {
                feedback = true;
            }

            if (LevelManagement.gameMode == 1)
            {
                if (angleDifference > thresholdMovementCanStart && angle < 0 && feedback == true)
                {
                    direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                    direction[2] = 0.0;

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if(feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCanStart && angle > 0 && feedback == true)
                {
                    direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                    direction[2] = 0.0;

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if (feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }
                    firstMove = false;
                }
            }
            else if (LevelManagement.gameMode == 2)
            {
                if (angleDifference > thresholdMovementCanStart && angle < 0 && feedback == true)
                {
                    direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = 0.0;
                    direction[2] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if (feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCanStart && angle > 0 && feedback == true)
                {
                    direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = 0.0;
                    direction[2] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if (feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }

                    firstMove = false;
                }
            }
            else if (LevelManagement.gameMode == 3)
            {
                if (angleDifference > thresholdMovementCanStart && angle < 0 && feedback == true)
                {
                    direction[0] = 0.0;
                    direction[1] = Mathf.Cos(angle * Mathf.Deg2Rad);
                    direction[2] = Mathf.Sin(angle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if (feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCanStart && angle > 0 && feedback == true)
                {
                    direction[0] = 0.0;
                    direction[1] = -Mathf.Cos(angle * Mathf.Deg2Rad);
                    direction[2] = -Mathf.Sin(angle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if (feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }

                    firstMove = false;
                }
            }
        }
    }

    void ForceFeedbackFromEverywereToNeutralPosition()
    {
        if (angleDifference > (thresholdMovementCompleteLeft + 30) && angle < 0 || angleDifference > (thresholdMovementCompleteRight + 30) && angle > 0)
        {
            HapticPlugin.setConstantForceValues("Default Device", direction, 0);

            feedbackCounter = feedbackCounterReset;
        }
        else
        {
            if (movementCanStart == true && firstMove == false)
            {
                HapticPlugin.setConstantForceValues("Default Device", direction, 0);

                feedbackCounter = feedbackCounterReset;

                firstMove = true;
            }

            if (LevelManagement.gameMode == 1)
            {
                if (angleDifference > thresholdMovementCanStart && angle < 0 && movementCanStart == false)
                {
                    direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                    direction[2] = 0.0;

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if (feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCanStart && angle > 0 && movementCanStart == false)
                {
                    direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                    direction[2] = 0.0;

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if (feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }

                    firstMove = false;
                }
            }
            else if (LevelManagement.gameMode == 2)
            {
                if (angleDifference > thresholdMovementCanStart && angle < 0 && movementCanStart == false)
                {
                    direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = 0.0;
                    direction[2] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if (feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCanStart && angle > 0 && movementCanStart == false)
                {
                    direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = 0.0;
                    direction[2] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if (feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }

                    firstMove = false;
                }
            }
            else if (LevelManagement.gameMode == 3)
            {
                if (angleDifference > thresholdMovementCanStart && angle < 0 && movementCanStart == false)
                {
                    direction[0] = 0.0;
                    direction[1] = Mathf.Cos(angle * Mathf.Deg2Rad);
                    direction[2] = Mathf.Sin(angle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if (feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCanStart && angle > 0 && movementCanStart == false)
                {
                    direction[0] = 0.0;
                    direction[1] = -Mathf.Cos(angle * Mathf.Deg2Rad);
                    direction[2] = -Mathf.Sin(angle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                    if (feedbackCounter < 1)
                    {
                        feedbackCounter += feedbackCounterIncrement;
                    }

                    firstMove = false;
                }
            }
        }
    }

    void ResistanceFromEverywereToNeutralPosition()
    {
        if (angleDifference > (thresholdMovementCompleteLeft + 30) && angle < 0 || angleDifference > (thresholdMovementCompleteRight + 30) && angle > 0)
        {
            HapticPlugin.setConstantForceValues("Default Device", direction, 0);
        }
        else
        {
            if (angleDifference < thresholdMovementCanStart && firstMove == false)
            {
                HapticPlugin.setConstantForceValues("Default Device", direction, 0);
                firstMove = true;
            }

            if (LevelManagement.gameMode == 1)
            {
                if (angleDifference > thresholdMovementCanStart && angle < 0)
                {
                    direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                    direction[2] = 0.0;

                    HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCanStart && angle > 0)
                {
                    direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                    direction[2] = 0.0;

                    HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                    firstMove = false;
                }
            }
            else if (LevelManagement.gameMode == 2)
            {
                if (angleDifference > thresholdMovementCanStart && angle < 0)
                {
                    direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = 0.0;
                    direction[2] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCanStart && angle > 0)
                {
                    direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                    direction[1] = 0.0;
                    direction[2] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                    firstMove = false;
                }
            }
            else if (LevelManagement.gameMode == 3)
            {
                if (angleDifference > thresholdMovementCanStart && angle < 0)
                {
                    direction[0] = 0.0;
                    direction[1] = -Mathf.Cos(angle * Mathf.Deg2Rad);
                    direction[2] = -Mathf.Sin(angle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                    firstMove = false;
                }

                if (angleDifference > thresholdMovementCanStart && angle > 0)
                {
                    direction[0] = 0.0;
                    direction[1] = Mathf.Cos(angle * Mathf.Deg2Rad);
                    direction[2] = Mathf.Sin(angle * Mathf.Deg2Rad);

                    HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                    firstMove = false;
                }
            }
        }
    }

    int GetNextFreeLane()
    {
        GameObject[] cars;
        cars = GameObject.FindGameObjectsWithTag("obstacle");

        float zLane1 = 1000;
        float zLane2 = 1000;
        float zLane3 = 1000;
        float zLane4 = 1000;

        foreach (GameObject car in cars)
        {
            if (car.transform.position.x == 0 && car.transform.position.z > 0)
            {
                if (zLane1 > car.transform.position.z)
                {
                    zLane1 = car.transform.position.z;
                }
            }

            if (car.transform.position.x == -LevelManagement.laneWidth && car.transform.position.z > 0)
            {
                if (zLane2 > car.transform.position.z)
                {
                    zLane2 = car.transform.position.z;
                }
            }

            if (car.transform.position.x == -LevelManagement.laneWidth * 2 && car.transform.position.z > 0)
            {
                if (zLane3 > car.transform.position.z)
                {
                    zLane3 = car.transform.position.z;
                }
            }

            float test = -LevelManagement.laneWidth * 3;

            if (car.transform.position.x == test && car.transform.position.z > 0)
            {
                if (zLane4 > car.transform.position.z)
                {
                    zLane4 = car.transform.position.z;
                }
            }
        }

        if (numberOfLanes <= 2)
        {
            if (zLane1 > zLane2)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }
        else if (numberOfLanes == 3)
        {
            if (zLane1 > zLane2 && zLane1 > zLane3)
            {
                return 1;
            }
            else if (zLane2 > zLane1 && zLane2 > zLane3)
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }
        else
        {
            if (zLane1 > zLane2 && zLane1 > zLane3 && zLane1 > zLane4)
            {
                return 1;
            }
            else if (zLane2 > zLane1 && zLane2 > zLane3 && zLane2 > zLane4)
            {
                return 2;
            }
            else if (zLane3 > zLane1 && zLane3 > zLane2 && zLane3 > zLane4)
            {
                return 3;
            }
            else
            {
                return 4;
            }
        }

    }

    void Resistance()
    {
        if (freeLane != GetNextFreeLane())
        {
            laneChanged = true;
            freeLane = GetNextFreeLane();
        }

        if (laneChangeLeft == true && wasInNeutralPosition == true || laneChangeRight == true && wasInNeutralPosition == true)
        {
            countdownForceFeedback = true;
            wasInNeutralPosition = false;
        }

        if (movementCanStart == true)
        {
            wasInNeutralPosition = true;
        }

        if (countdownForceFeedback == true)
        {
            countdownLaneChange -= Time.deltaTime;

            if (countdownLaneChange < 0)
            {
                countdownForceFeedback = false;
                countdownLaneChange = countdownLaneChangeReset;
                feedbackPossible = true;
            }
        }

        if (feedbackPossible == true)
        {
            ResistanceFromEverywereToNeutralPosition();
        }

        if (angleDifference < thresholdMovementCanStart && feedbackPossible == true)
        {
            feedbackPossible = false;
            laneChanged = true;
        }

        if (laneChanged == true)
        {
            countdownLaneChange -= Time.deltaTime;

            if (countdownLaneChange < 0)
            {
                laneChanged = false;
                countdownLaneChange = countdownLaneChangeReset;
            }
        }
        else
        {
            if (movementCanStart == true)
            {
                feedbackMovementSupport = true;
            }
            else
            {
                feedbackMovementSupport = false;

                if (firstMoveMovementSupport == false)
                {
                    HapticPlugin.setConstantForceValues("Default Device", direction, 0);
                    firstMoveMovementSupport = true;
                }
            }

            if (freeLane < currentLane)
            {
                if (angleDifference > thresholdMovementCompleteLeft && angle < 0 && firstMoveMovementSupport == false)
                {
                    HapticPlugin.setConstantForceValues("Default Device", direction, 0);
                    firstMoveMovementSupport = true;
                    feedbackMovementSupport = false;

                }

                if (LevelManagement.gameMode == 1)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                        direction[1] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                        direction[2] = 0.0;

                        HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                        firstMoveMovementSupport = false;
                    }
                }
                else if (LevelManagement.gameMode == 2)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                        direction[1] = 0.0;
                        direction[2] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                        HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                        firstMoveMovementSupport = false;
                    }
                }
                else if (LevelManagement.gameMode == 3)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = 0.0;
                        direction[1] = -Mathf.Cos(angle * Mathf.Deg2Rad);
                        direction[2] = -Mathf.Sin(angle * Mathf.Deg2Rad);

                        HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                        firstMoveMovementSupport = false;
                    }
                }
            }
            else if (freeLane > currentLane)
            {
                if (angleDifference > thresholdMovementCompleteRight && angle > 0 && firstMoveMovementSupport == false)
                {
                    HapticPlugin.setConstantForceValues("Default Device", direction, 0);
                    firstMoveMovementSupport = true;
                    feedbackMovementSupport = false;

                }

                if (LevelManagement.gameMode == 1)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                        direction[1] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                        direction[2] = 0.0;

                        HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                        firstMoveMovementSupport = false;
                    }
                }
                else if (LevelManagement.gameMode == 2)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                        direction[1] = 0.0;
                        direction[2] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                        HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                        firstMoveMovementSupport = false;
                    }
                }
                else if (LevelManagement.gameMode == 3)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = 0.0;
                        direction[1] = Mathf.Cos(angle * Mathf.Deg2Rad);
                        direction[2] = Mathf.Sin(angle * Mathf.Deg2Rad);

                        HapticPlugin.setConstantForceValues("Default Device", direction, feedbackForce);

                        firstMoveMovementSupport = false;
                    }
                }
            }
            else
            {
                if (firstMoveMovementSupport == false)
                {
                    HapticPlugin.setConstantForceValues("Default Device", direction, 0);
                    firstMoveMovementSupport = true;
                    feedbackMovementSupport = false;

                }
            }
        }
    }

    void MovementSupport()
    {
        if (freeLane != GetNextFreeLane())                                                                                      //checks if next free lane has changed
        {
            laneChanged = true;
            freeLane = GetNextFreeLane();
        }

        if (laneChangeLeft == true && wasInNeutralPosition == true || laneChangeRight == true && wasInNeutralPosition == true)  //checks if support back to neutral position should start
        {
            countdownForceFeedback = true;
            wasInNeutralPosition = false;
        }

        if (movementCanStart == true)                                                                                           //makes sure support back to neutral position doesn't start
        {                                                                                                                       //before patient was in neutral position
            wasInNeutralPosition = true;
        }

        if (countdownForceFeedback == true)                                                                                     //countdown for support back to neutral position
        {
            countdownLaneChange -= Time.deltaTime;

            if (countdownLaneChange < 0)
            {
                countdownForceFeedback = false;
                countdownLaneChange = countdownLaneChangeReset;
                feedbackPossible = true;
            }
        }

        if (feedbackPossible == true)                                                                                           //support back to neutral position
        {
            ForceFeedbackFromEverywereToNeutralPosition();
        }

        if (movementCanStart == true && feedbackPossible == true)                                                               //checks if support back to neutral position should end
        {
            feedbackPossible = false;
            laneChanged = true;
            countdownForceFeedback = false;
            countdownLaneChange = countdownLaneChangeReset;
        }

        if (laneChanged == true)                                                                                                //countdown for movement support to the side
        {
            countdownLaneChange -= Time.deltaTime;

            if (countdownLaneChange < 0)
            {
                laneChanged = false;
                countdownLaneChange = countdownLaneChangeReset;
            }
        }
        else
        {
            if (movementCanStart == true)                                                                                       //makes sure movement to the side does not happen while patient 
            {                                                                                                                   //comes back to neutral position
                feedbackMovementSupport = true;
            }
            else
            {
                feedbackMovementSupport = false;
            }

            if (freeLane > currentLane)
            {
                if (angleDifference > thresholdMovementCompleteLeft && angle < 0 && firstMoveMovementSupport == false)
                {
                    HapticPlugin.setConstantForceValues("Default Device", direction, 0);

                    feedbackCounter = feedbackCounterReset;

                    firstMoveMovementSupport = true;
                    feedbackMovementSupport = false;
                }

                if (LevelManagement.gameMode == 1)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                        direction[1] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                        direction[2] = 0.0;

                        HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                        if (feedbackCounter < 1)
                        {
                            feedbackCounter += feedbackCounterIncrement;
                        }

                        firstMoveMovementSupport = false;
                    }
                }
                else if (LevelManagement.gameMode == 2)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = -Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                        direction[1] = 0.0;
                        direction[2] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                        HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                        if (feedbackCounter < 1)
                        {
                            feedbackCounter += feedbackCounterIncrement;
                        }

                        firstMoveMovementSupport = false;
                    }
                }
                else if (LevelManagement.gameMode == 3)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = 0.0;
                        direction[1] = -Mathf.Cos(angle * Mathf.Deg2Rad);
                        direction[2] = -Mathf.Sin(angle * Mathf.Deg2Rad);

                        HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                        if (feedbackCounter < 1)
                        {
                            feedbackCounter += feedbackCounterIncrement;
                        }

                        firstMoveMovementSupport = false;
                    }
                }
            }
            else if (freeLane < currentLane)
            {
                if (angleDifference > thresholdMovementCompleteRight && angle > 0 && firstMoveMovementSupport == false)
                {
                    HapticPlugin.setConstantForceValues("Default Device", direction, 0);

                    feedbackCounter = feedbackCounterReset;

                    firstMoveMovementSupport = true;
                    feedbackMovementSupport = false;

                }

                if (LevelManagement.gameMode == 1)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                        direction[1] = Mathf.Sin(currentAngle * Mathf.Deg2Rad);
                        direction[2] = 0.0;

                        HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                        if (feedbackCounter < 1)
                        {
                            feedbackCounter += feedbackCounterIncrement;
                        }

                        firstMoveMovementSupport = false;
                    }
                }
                else if (LevelManagement.gameMode == 2)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = Mathf.Cos(currentAngle * Mathf.Deg2Rad);
                        direction[1] = 0.0;
                        direction[2] = -Mathf.Sin(currentAngle * Mathf.Deg2Rad);

                        HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                        if (feedbackCounter < 1)
                        {
                            feedbackCounter += feedbackCounterIncrement;
                        }

                        firstMoveMovementSupport = false;
                    }
                }
                else if (LevelManagement.gameMode == 3)
                {
                    if (feedbackMovementSupport == true)
                    {
                        direction[0] = 0.0;
                        direction[1] = Mathf.Cos(angle * Mathf.Deg2Rad);
                        direction[2] = Mathf.Sin(angle * Mathf.Deg2Rad);

                        HapticPlugin.setConstantForceValues("Default Device", direction, (feedbackForce * feedbackCounter));

                        if (feedbackCounter < 1)
                        {
                            feedbackCounter += feedbackCounterIncrement;
                        }

                        firstMoveMovementSupport = false;
                    }
                }
            }
            else
            {
                if (firstMoveMovementSupport == false)
                {
                    HapticPlugin.setConstantForceValues("Default Device", direction, 0);

                    feedbackCounter = feedbackCounterReset;

                    firstMoveMovementSupport = true;
                    feedbackMovementSupport = false;

                }
            }
        }
    }

    void RotationOnCircle() //unused
    {
        float[] xRotation = new float[181];
        float[] yRotation = new float[181];

        GameObject hapticObject = GameObject.Find("HapticCollider");

        float absoluteDistance = 100;
        int rotationAngle = 0;

        for (int i = 0; i < 91; i++)
        {
            xRotation[i] = (-0.03f) - Mathf.Cos(i * Mathf.Deg2Rad) * 0.06f;
            yRotation[i] = 0.015f - Mathf.Sin(i * Mathf.Deg2Rad) * 0.06f;
        }

        for (int i = 91; i < 181; i++)
        {
            xRotation[i] = (-0.03f) - Mathf.Cos(i * Mathf.Deg2Rad) * 0.06f;
            yRotation[i] = 0.015f - Mathf.Sin(i * Mathf.Deg2Rad) * 0.06f;
        }

        for (int i = 0; i < 181; i++)
        {
            if (absoluteDistance > Mathf.Sqrt(Mathf.Pow(xRotation[i] - hapticObject.transform.position.x, 2) + Mathf.Pow(yRotation[i] - hapticObject.transform.position.y, 2)))
            {
                absoluteDistance = Mathf.Sqrt(Mathf.Pow(xRotation[i] - hapticObject.transform.position.x, 2) + Mathf.Pow(yRotation[i] - hapticObject.transform.position.y, 2));
                rotationAngle = i;
            }

        }

        if (absoluteDistance > 0.01)
        {
            double[] forcedirection = new double[3];

            forcedirection[0] = (xRotation[rotationAngle] - hapticObject.transform.position.x) * -100;
            forcedirection[1] = (yRotation[rotationAngle] - hapticObject.transform.position.y) * 100;
            forcedirection[2] = 0;

            Debug.LogError("distance: " + absoluteDistance + " angle: " + rotationAngle + " x: " + forcedirection[0] + " y: " + forcedirection[1]);

            HapticPlugin.setConstantForceValues("Default Device", forcedirection, 0.1);
        }
        else
        {
            double[] forcedirection = new double[3];

            forcedirection[0] = 0;
            forcedirection[1] = 0;
            forcedirection[2] = 0;

            HapticPlugin.setConstantForceValues("Default Device", forcedirection, 0);
            Debug.LogError("distance: " + absoluteDistance + " angle: " + rotationAngle);
        }

    }
}