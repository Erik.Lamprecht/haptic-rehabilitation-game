using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate_with_haptic_device : MonoBehaviour
{
    public GameObject HapticDevice;

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.rotation = HapticDevice.transform.rotation;
    }
}
