using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement_obstacle : MonoBehaviour
{
    private GameObject[] obstacles;
    private int numberOfObstacles;
    private float speed;
    private float position;
    private GameObject[] otherCars;
    private int numberOfLanes;
    private float minimalDistanceBetweenObstacles = 50f;
    private float variationDistanceBetweenObstacles = 5f;
    private int randomLane;
    private int lastRandomeLane;
    private bool sameLane = false;
    private float randomDistanceBetweenObstacles;
    private Vector3 positionOfCar;
    private GameObject randomCarObject;
    private MeshFilter meshFilter;
    private string meshName;
    private float heightOfCar;


    // Start is called before the first frame update
    void Start()
    {
        obstacles = Resources.LoadAll<GameObject>("obstacles");                               //Finds all Prefabs in Assets/Resources/Prefabs

        numberOfObstacles = obstacles.Length;

        numberOfLanes = (int)LevelManagement.levelParameters[LevelManagement.currentLevel, 1];

        speed = LevelManagement.levelParameters[LevelManagement.currentLevel, 5];
    }


    // Update is called once per frame
    void Update()
    {
        if (LevelManagement.gameHasStarted == true)
        {
            gameObject.transform.Translate(0f, 0f, -speed * Time.deltaTime, Space.World);                    //constantly travels onto the camera

            if (gameObject.transform.position.z < -35)
            {
                otherCars = GameObject.FindGameObjectsWithTag("obstacle");

                foreach (GameObject car in otherCars)
                {
                    if (car.transform.position.z > position)                               //finds last obstacle
                    {
                        position = car.transform.position.z;
                    }

                    if (car.transform.position.z < -25 && car != gameObject)
                    {
                        Destroy(car);
                    }

                }

                position += minimalDistanceBetweenObstacles;

                randomLane = Random.Range(0, numberOfLanes);                                    //chooses which lane stays free

                for (int i = 0; i < numberOfLanes; i++)
                {
                    while (randomLane == lastRandomeLane && sameLane == true)                       //makes sure the same lane isn't coosen more than 2
                    {                                                                               //times in a row
                        randomLane = Random.Range(0, numberOfLanes);
                    }

                    if (i != randomLane)
                    {
                        randomCarObject = obstacles[Random.Range(0, numberOfObstacles)];        //chooses random car                      

                        meshFilter = randomCarObject.GetComponent<MeshFilter>();

                        meshName = meshFilter.sharedMesh.name;

                        if (string.Equals(meshName, "Bus"))                                      //adjusts hight for different cars/trucks/busses
                            heightOfCar = 1.627174f;
                        else if (string.Equals(meshName, "Truck 1"))
                            heightOfCar = 1.983313f;
                        else if (string.Equals(meshName, "Truck 2"))
                            heightOfCar = 1.766092f;
                        else if (string.Equals(meshName, "Car 1"))
                            heightOfCar = 0.8485862f;
                        else if (string.Equals(meshName, "Car 2"))
                            heightOfCar = 0.7575706f;
                        else if (string.Equals(meshName, "Car 3"))
                            heightOfCar = 0.7864221f;
                        else if (string.Equals(meshName, "Car 4"))
                            heightOfCar = 1.262459f;
                        else if (string.Equals(meshName, "Car 5"))
                            heightOfCar = 0.981249f;
                        else if (string.Equals(meshName, "Car 6"))
                            heightOfCar = 0.8591423f;

                        randomDistanceBetweenObstacles = Random.Range(0, variationDistanceBetweenObstacles);    //chooses distance variation

                        positionOfCar.Set(-(float)LevelManagement.laneWidth * i, heightOfCar, position + randomDistanceBetweenObstacles);

                        Instantiate(randomCarObject, positionOfCar, gameObject.transform.rotation);             //spawns car

                        Destroy(gameObject);
                    }
                }

                if (randomLane == lastRandomeLane)                                                               //checks if same lane was free twice in a row
                {
                    sameLane = true;
                }
                else
                {
                    sameLane = false;
                    lastRandomeLane = randomLane;
                }
            }
        }
    }
}

