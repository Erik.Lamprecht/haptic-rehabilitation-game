using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movemnt_of_enviroment : MonoBehaviour
{
    private float speed = 30;
    private float distance;
    private float position;
    private GameObject[] roads;
    
    void Start()
    {
        roads = GameObject.FindGameObjectsWithTag("road");                                     //find all rode parts
    }


    // Update is called once per frame
    void Update()
    {
        if (LevelManagement.gameHasStarted == true)
        {
            gameObject.transform.Translate(0f, 0f, -speed * Time.deltaTime);                    //constantly travels onto the camera

            if (gameObject.transform.position.z < -40)
            {
                foreach (GameObject road in roads)
                {
                    if (road.transform.position.z > position)                                   //finds last road object
                    {
                        position = road.transform.position.z;
                    }
                }

                distance = position - gameObject.transform.position.z + 29f;

                gameObject.transform.Translate(0f, 0f, distance);                               //travels distance between own 
                                                                                                //position and last road part 
                position = 0f;                                                                  //+ 30 (length of a road part)
            }
        }
    }
}
